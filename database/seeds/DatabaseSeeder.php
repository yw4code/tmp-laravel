<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Customer;
use App\Currency;
use App\Order;

class DatabaseSeeder extends Seeder
{

    const CATEGORY_RANGE = 5;
    const CUSTOMER_RANGE = 20;
    const ORDER_RANGE = 50;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        for($i= 1; $i<= DatabaseSeeder::CATEGORY_RANGE; $i++) {
            Category::create(['name' => 'category'.$i]);
        }

        for($i= 1; $i<= DatabaseSeeder::CUSTOMER_RANGE; $i++) {
            Customer::create(['name' => 'category'.$i]);
        }

        Currency::create(['name' => 'USD', 'rate' => 1, 'created_at' => '2018-07-01 00:00:00', 'updated_at' => '2018-07-01 00:00:00']);
        Currency::create(['name' => 'TWD', 'rate' => 30.76, 'created_at' => '2018-07-01 00:00:00', 'updated_at' => '2018-07-01 00:00:00']);
        Currency::create(['name' => 'TWD', 'rate' => 30.76, 'created_at' => '2018-08-01 00:00:00', 'updated_at' => '2018-08-01 00:00:00']);

        for ($i = 1; $i <= DatabaseSeeder::ORDER_RANGE; $i++) {

            $isPaid = rand(0,1);

            Order::create([
                'number' => 'ORDER'.$i,
                'price' => rand(1000, 10000),
                'category_id' => rand(1, DatabaseSeeder::CATEGORY_RANGE),
                'customer_id' => rand(1, DatabaseSeeder::CUSTOMER_RANGE),
                'currency_id' => 1,
                'financial_date' => date('Y-m-d H:i:s', time()-60*60*24*3),
                'invoice_date' => date('Y-m-d H:i:s', time()-60*60*24*3),
                'payment_date' => ($isPaid)?date('Y-m-d H:i:s'):null,
                'payment_method' => ($isPaid)?'credit':null,
            ]);
        }
    }
}
