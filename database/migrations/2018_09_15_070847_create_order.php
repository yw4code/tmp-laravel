<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function(Blueprint $table) {
            $table->Increments('id');
            $table->string('number', 20)->unique()->comment("Name of record");

            $table->decimal('price', 20, 10);

            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('currency_id')->unsigned();

            $table->timestamp('financial_date')->nullable();
            $table->timestamp('invoice_date')->nullable();

            $table->timestamp('payment_date')->nullable();
            $table->enum('payment_method', ['credit', 'cash'])->nullable();


        });

        Schema::table('order', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')->on('category')
                ->onDelete('set null');
            $table->foreign('customer_id')
                ->references('id')->on('customer')
                ->onDelete('set null');


            $table->index('financial_date');
            $table->index('invoice_date');
            $table->index('payment_date');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
