<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @param Request $request
     *      financial_month e.g. 2018-05
     */
    public function index(Request $request) {

        $order = Order::query();
        if ($request->has('financial_month')) {
            $order->where('financial_date', 'like', $request->financial_month.'%');
        }

        return response()->json($order->get());
    }

    /**
     * @param Request $request
     *      id
     *      data
     *
     */
    public function update(Request $request) {
        Order::where('id', $request->id)->update($request->data);
    }

    /**
     * @param Request $request
     *  id
     */
    public function delete(Request $request) {
        Order::where('id', $request->id)->delete();
    }
}
